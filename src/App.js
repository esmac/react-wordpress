import React from 'react';
import Home from "./components/Home";
import {Router} from "@reach/router";
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from "./components/NavBar";
import Admin from "./components/admin";

class App extends React.Component {
    render() {
        return (
            <div>
                <NavBar/>
                <Router>
                    <Home path={'/'}/>
                </Router>
                <Router>
                    <Admin path={'/admin'}/>
                </Router>
            </div>
        );
    }
}

export default App;
