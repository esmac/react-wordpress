import React from 'react';
import 'react-dropzone-uploader/dist/styles.css'
import Dropzone from 'react-dropzone-uploader'
import axios from "axios";

class UploadResume extends React.Component {
    constructor(props) {
        super(props);
    }

    getUploadParams = ({file, meta}) => {
        // console.log('meta', meta);
        // console.log('FILE', file);
        let fileBase64 = '';
        this.getBase64(file, (result) => {
            fileBase64 = result;
        });
        return {
            url: 'http://localhost:8080/wordpress/wp-json/api/v1/upload/resume',
            fields: {
                file: fileBase64,
            }
        }
    };
    handleDrop = (file) => {
        let fileBase64 = '';
        // console.log(file[0].file);
        const params = {
            file: 0
        }
        this.getBase64(file[0].file, (result) => {
            params.file = result;
            this.uploadFile(params);
        });
    }
    uploadFile (params) {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        axios.post(`${baseUrl}upload/resume`, params).then(res => {
            if (res.data) {
                this.props.onUploadResume(res.data);
            }
        });
    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    // handleSubmit = (files) => { console.log('FILE=', files.map(f => f.meta)) }
    render() {
        return (
            <Dropzone
                // getUploadParams={this.getUploadParams}
                // onChangeStatus={this.props.onUploadResume}
                // onDrop={this.handleDrop}
                onSubmit={this.handleDrop}
                maxFiles={1}
                accept="image/*"
            />
        )
    }
}

export default UploadResume;
