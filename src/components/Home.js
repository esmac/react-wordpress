import React from 'react';
import NavBar from "./NavBar";
import axios from 'axios';
import {Link} from '@reach/router';
import UploadResume from "./UploadResume";
import DetailsForm from "./detailsForm";
import Loader from '../loader.gif';
import '../style.css';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            domains: [],
            selectedDomainId: 0,
            viewHome: true,
            viewUploadResume: false,
            viewForm: false,
            thankYou: false,
            error: '',
            resumeUrl: '',
            name: '',
            email: '',
            number: '',
            address: '',
            discipline: '',
            gradYear: '',
            university: '',
            skills: ''

        }
        this.setDocumentUrl = this.setDocumentUrl.bind(this);
        this.setFormData = this.setFormData.bind(this);
        this.onClickSave = this.onClickSave.bind(this);
    }

    componentDidMount() {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        this.setState({loading: true}, () => {
            axios.get(`${baseUrl}domain`)
                .then(res => {
                        this.setState({loading: false, domains: res.data});
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    setDomainId(domainId) {
        this.setState({selectedDomainId: domainId, viewHome: false, viewUploadResume: true});
    }

    setDocumentUrl(document) {
        // console.log(document, this.state);
        this.setState({resumeUrl: document, viewUploadResume: false, viewForm: true})
        /*if (status === 'done') {
            // console.log('meta: ', meta, 'file', file, 'xhr', xhr, 'state', status);
            console.log('XHR', xhr);
        }*/
    }

    setFormData(key, value) {
        switch (key) {
            case 'name' :
                this.setState({name: value});
                break;
            case 'email':
                this.setState({email: value});
                break;
            case 'number':
                this.setState({number: value});
                break;
            case 'address':
                this.setState({address: value});
                break;
            case 'discipline':
                this.setState({discipline: value});
                break;
            case 'gradYear':
                this.setState({gradYear: value});
                break;
            case 'uni':
                this.setState({university: value});
                break;
            case 'skills':
                this.setState({skills: value});
                break;
        }
    }

    onClickSave() {
        let params = {
            name: this.state.name,
            email: this.state.email,
            number: this.state.number,
            address: this.state.address,
            dicipline: this.state.discipline,
            grad_year: this.state.gradYear,
            university: this.state.university,
            skills: this.state.skills,
            cv: this.state.resumeUrl,
            domain_id: this.state.selectedDomainId
        };
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        this.setState({loading: true}, () => {
            axios.post(`${baseUrl}create/user`, params).then(res => {
                if (res.data) {
                    this.setState({loading: false, viewForm: false, thankYou: true});
                }
            }).catch(err => {
                this.setState({loading: false});
                console.log('ERR', err)
            })
        })
    }

    render() {
        const {domains} = this.state;
        return (
            <div>
                <div className="mt-5 justify-content-center row">
                    {(domains.length && this.state.viewHome) && (
                        domains.map(domain => (
                            <div key={domain.domain_id} className="col-md-2 cursor-pointer">
                                <div onClick={() => {
                                    this.setDomainId(domain.domain_id)
                                }}>
                                    <div className="p-2 border-dark card">
                                        {domain.domain_name}
                                    </div>
                                </div>
                            </div>
                        ))
                    )}
                    {this.state.viewUploadResume &&
                    <UploadResume onUploadResume={this.setDocumentUrl}/>
                    }
                    {this.state.viewForm &&
                    <DetailsForm onSetFormData={this.setFormData} onSubmitForm={this.onClickSave}/>
                    }
                    {this.state.thankYou &&
                    <div>
                        <h1>Thank You!</h1>
                        <h3>Your Form has been submitted.</h3>
                        <button className={'btn btn-success'} onClick={() => {
                            this.setState({thankYou: false, viewHome: true})
                        }}>OK
                        </button>
                    </div>
                    }
                    {this.state.loading &&
                    <img src={Loader} className={'loader'}/>
                    }
                </div>
            </div>
        )
    }
}

export default Home;
