import React from "react";
import axios from "axios";
import Loader from '../loader.gif';
import '../style.css';
import Table from "react-bootstrap/Table";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
import {Pie} from "react-chartjs-2";

class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            viewAll: true,
            viewShortList: false,
            error: '',
            users: [],
            showModal: false,
            user: '',
            domains: [],
            labels: [],
            datasets: [{
                data: [10, 20, 15, 10],
                backgroundColor: ['red', 'blue', 'green', 'yellow']
            }],
            viewChart: false
        }
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal() {
        this.setState({showModal: false});
    }

    showModal(user) {
        this.setState({user: user, showModal: true})
    }

    componentDidMount() {
        this.getAllDomains();
    }

    getAllDomains() {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        this.setState({loading: true}, () => {
            axios.get(`${baseUrl}domain`)
                .then(res => {
                        this.setState({loading: false, domains: res.data}, () => {
                            res.data.forEach(domain => {
                                this.state.labels.push(domain.domain_name);
                            });
                        });
                        this.fetchAllUsers();
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    fetchAllUsers() {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        this.setState({loading: true}, () => {
            axios.get(`${baseUrl}all/user`)
                .then(res => {
                        this.setState({loading: false, users: res.data, viewAll: true, viewShortList: false}, () => {
                            /*this.state.domains.forEach((domain, i) => {
                                this.state.datasets[0].data.push(0);
                                console.log('user.domain_id == domain.domain_id ', user.domain_id == domain.domain_id);
                                res.data.forEach((user) => {
                                    if (user.domain_id == domain.domain_id) {
                                        this.state.datasets[0].data[i]++;
                                    }
                                });
                            });
                            console.log(this.state);*/
                            this.state.viewChart = true;
                        });
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    fetchShortListedUser() {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        this.setState({loading: true}, () => {
            axios.get(`${baseUrl}fetch/shortlist/user`)
                .then(res => {
                        this.setState({loading: false, users: res.data, viewAll: false, viewShortList: true});
                        console.log(this.state)
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    deleteUser(user_id) {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        const params = {
            user_id
        };
        this.setState({loading: true}, () => {
            axios.put(`${baseUrl}delete/user`, params)
                .then(res => {
                        this.setState({loading: false});
                        this.fetchAllUsers();
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    shortList(user_id) {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        const params = {
            user_id
        };
        this.setState({loading: true}, () => {
            axios.put(`${baseUrl}shortlist/user`, params)
                .then(res => {
                        this.setState({loading: false});
                        this.fetchAllUsers();
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    emailShortList(user_id) {
        const baseUrl = 'http://localhost:8080/wordpress/wp-json/api/v1/';
        const params = {
            user_id
        };
        this.setState({loading: true}, () => {
            axios.put(`${baseUrl}email/shortlist`, params)
                .then(res => {
                        this.setState({loading: false});
                    }
                ).catch(err => {
                    this.setState({loading: false, error: err.response.data});
                }
            )
        });
    }

    render() {
        return (
            <div className={'p-5'}>
                <Nav variant="pills" className={'mb-3'}>
                    <Nav.Item>
                        <Nav.Link className={this.state.viewAll ? 'active' : ''} onClick={() => {
                            this.fetchAllUsers()
                        }}>All Candidates</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link className={this.state.viewShortList ? 'active' : ''} onClick={() => {
                            this.fetchShortListedUser()
                        }}>ShortListed Candidates</Nav.Link>
                    </Nav.Item>
                </Nav>
                {this.state.viewChart === true ? (
                    <Pie data={{
                        labels: this.state.labels,
                        datasets: this.state.datasets
                    }} height='50%'/>
                ) : ''}
                <Table striped bordered hover size="sm" variant='dark'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Number</th>
                        <th>Domain Applied</th>
                        <th>CV</th>
                        <th>Show Information</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {(this.state.users.length) ? (
                        this.state.users.map((user, index) => (
                            <tr key={user.user_id}>
                                <td>{index + 1}</td>
                                <td>{user.name}</td>
                                <td>{user.email}</td>
                                <td>{user.number}</td>
                                <td>{user.domain_name}</td>
                                <td><a href={`http://localhost:8080/wordpress/` + user.cv} target={'_blank'}><i
                                    className="fa fa-download"></i></a></td>
                                <td className={'text-underline cursor-pointer'} onClick={() => {
                                    this.showModal(user);
                                }}>Details
                                </td>
                                <td><i className="fa fa-trash" onClick={() => {
                                    this.deleteUser(user.user_id)
                                }}></i>
                                    {user.shortlisted == 0 ? (
                                        <p className={'text-underline d-block cursor-pointer'} onClick={() => {
                                            this.shortList(user.user_id)
                                        }}>ShortList Candidate</p>
                                    ) : ''}
                                    {user.shortlisted == 1 ? (
                                        <p className={'text-underline d-block cursor-pointer'} onClick={() => {
                                            this.emailShortList(user.user_id)
                                        }}>Email Candidate</p>
                                    ) : ''}
                                </td>
                            </tr>
                        ))) : (
                        <tr colSpan='13'>
                            <td colSpan='13'><span>No Record found!</span></td>
                        </tr>
                    )
                    }
                    </tbody>
                </Table>
                <Modal show={this.state.showModal} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Candidate Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5>Personal Information</h5>
                        <div className={'ml-5 mr-5'}>
                            <label className={'d-block'}>Name: {this.state.user.name}</label>
                            <label className={'d-block'}>Contact Number: {this.state.user.number}</label>
                            <label className={'d-block'}>Email: {this.state.user.email}</label>
                            <label className={'d-block'}>Address: {this.state.user.address}</label>
                        </div>
                        <h5>Personal Information</h5>
                        <div className={'ml-5 mr-5'}>
                            <label className={'d-block'}>Discipline: {this.state.user.dicipline}</label>
                            <label className={'d-block'}>Graduation year: {this.state.user.grad_year}</label>
                            <label className={'d-block'}>University: {this.state.user.university}</label>
                        </div>
                        <h5>Technical Background</h5>
                        <div className={'ml-5 mr-5'}>
                            <label className={'d-block'}>Skills: {this.state.user.skills}</label>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>

                {this.state.loading &&
                <img src={Loader} className={'loader'}/>
                }
            </div>
        )
    }
}

export default Admin;
