import React from 'react';
import Axios from "axios";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

class DetailsForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Form>
                    <h2>Personal Information</h2>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter name" onChange={(event) => {this.props.onSetFormData('name', event.target.value)}}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" onChange={(event) => {this.props.onSetFormData('email', event.target.value)}}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicNumber">
                        <Form.Label>Contact Number</Form.Label>
                        <Form.Control type="text" placeholder="Enter Contact Number" onChange={(event) => {this.props.onSetFormData('number', event.target.value)}}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicAddress">
                        <Form.Label>Address</Form.Label>
                        <Form.Control type="text" placeholder="Enter Address" onChange={(event) => {this.props.onSetFormData('address', event.target.value)}}/>
                    </Form.Group>
                    <h2>Educational Background</h2>
                    <Form.Group controlId="formBasicDiscipline">
                        <Form.Label>Discipline</Form.Label>
                        <Form.Control type="text" placeholder="Enter Discipline" onChange={(event) => {this.props.onSetFormData('discipline', event.target.value)}}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicGradYear">
                        <Form.Label>Graduation Year</Form.Label>
                        <Form.Control type="text" placeholder="Enter Graduation Year" onChange={(event) => {this.props.onSetFormData('gradYear', event.target.value)}}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicUniversity">
                        <Form.Label>University</Form.Label>
                        <Form.Control type="text" placeholder="Enter University" onChange={(event) => {this.props.onSetFormData('uni', event.target.value)}}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicSkills">
                        <Form.Label>Skills</Form.Label>
                        <Form.Control type="text" placeholder="Enter Skills" onChange={(event) => {this.props.onSetFormData('skills', event.target.value)}}/>
                        <Form.Text className="text-muted">
                            Enter Skills Followed by ','
                        </Form.Text>
                    </Form.Group>
                    <Button variant="primary" onClick={() => {this.props.onSubmitForm()}}>
                        Submit
                    </Button>
                </Form>
            </div>
        )
    }
}
export default DetailsForm;
