# React Workshop

:fire: This is a assignment for learning how to build React Application with wordpress.

## Branch Description:
Set up React Application with using Webpack and Babel

## Installation

1. Clone this repo in `git clone https://esmac@bitbucket.org/esmac/react-wordpress.git
                       
`

2. `cd react-wordpress`

3. Run `npm install`

## Commands

- `dev` Runs webpack dev server for development ( in watch mode )
- `prod` Runs webpack in production mode

